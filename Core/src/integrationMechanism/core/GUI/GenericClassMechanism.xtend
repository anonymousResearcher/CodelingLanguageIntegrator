package integrationMechanism.core.GUI

import org.codeling.mechanisms.ClassMechanism
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment

class GenericClassMechanism extends ClassMechanism {

	override getName() {
		return "Custom Translation";
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val EClass eClass = element as EClass;
		val String eClassName = eClass.name;
		val String eClassInstantiation = getEClassInstantiation(eClass);

		val content = '''
		package «packageFragment.elementName»;
		
		import java.util.List;
		
		import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
		import org.eclipse.emf.ecore.EObject;
		import org.eclipse.jdt.core.IJavaElement;
		
		import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
		import org.codeling.utils.CodelingException;
		import «eClass.packageName».«eClass.packageName»Package;
		import «eClass.packageName».«eClass.name»;
		
		«IF eClass.requiresFeaturePackageImport»
			import «packageFragment.elementName».«typeName.toLowerCase»_feature.*;
		«ENDIF»
		
		public class «typeName»Transformation extends ClassMechanismTransformation<«eClassName», IJavaElement> {
		
			public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
				super(parentTransformation, «eClassInstantiation»);
			}
		
			«createCrossReferencesTrasformations»
			
			«createChildTransformationsToCode»
		
			«createChildTransformationsToModel»
		
			@Override
			public void createCodeFragments() throws CodelingException {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void updateCodeFragments() throws CodelingException {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void deleteCodeFragments() throws CodelingException {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public «eClassName» transformToModel() throws CodelingException {
				// TODO Auto-generated method stub
				return null;
			}
		}''';

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return true;
	}

}
