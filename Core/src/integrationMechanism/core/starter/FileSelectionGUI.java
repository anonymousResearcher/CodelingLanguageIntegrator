package integrationMechanism.core.starter;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import integrationMechanism.core.generator.IntegrationMechanismCodeGenerator;

public class FileSelectionGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private final IntegrationMechanismCodeGenerator generatorClass;
	private JLabel lblNewLabel;
	private JFrame myFrame;

	public FileSelectionGUI(IntegrationMechanismCodeGenerator generatorClass) {
		this.generatorClass = generatorClass;
		createFrame();
		createLabels();
		createTextfiels();
		createButton();

	}

	private void createFrame() {
		setResizable(false);
		myFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 144);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}

	private void createTextfiels() {
		textField = new JTextField();
		textField.addMouseListener(modelChooseListener);
		textField.setBounds(139, 11, 244, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.addMouseListener(saveFolderChooseListener);
		textField_1.setBounds(139, 42, 244, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
	}

	MouseAdapter modelChooseListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			final JFileChooser chooser = new JFileChooser();
			final FileNameExtensionFilter filter = new FileNameExtensionFilter("Ecore Metamodelle", "ecore");
			chooser.setFileFilter(filter);
			final int returnVal = chooser.showOpenDialog(contentPane);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
				textField.setText(chooser.getSelectedFile().getAbsolutePath());
			}
		}
	};

	MouseAdapter saveFolderChooseListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			final JFileChooser chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			final int returnVal = chooser.showOpenDialog(contentPane);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
				textField_1.setText(chooser.getSelectedFile().getAbsolutePath());
			}
		}
	};

	private void createLabels() {
		lblNewLabel = new JLabel("Ecore-Model\r\n");
		lblNewLabel.setBounds(10, 14, 119, 14);
		contentPane.add(lblNewLabel);
		final JLabel lblNewLabel_2 = new JLabel("Save Folder");
		lblNewLabel_2.setBounds(10, 45, 108, 14);
		contentPane.add(lblNewLabel_2);

	}

	private void createButton() {
		final JButton btnNewButton = new JButton("Load Ecore-Model");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				generatorClass.loadModel(textField.getText());
				myFrame.dispose();
			}
		});
		btnNewButton.setBounds(257, 84, 128, 23);
		contentPane.add(btnNewButton);

	}
}