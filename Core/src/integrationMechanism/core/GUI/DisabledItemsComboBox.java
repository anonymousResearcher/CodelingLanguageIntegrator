package integrationMechanism.core.GUI;

import java.awt.Component;
import java.util.HashSet;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import org.codeling.mechanisms.Mechanism;

import integrationMechanism.core.generator.IntegrationMechanismCodeGenerator;

public class DisabledItemsComboBox extends JComboBox {

	public DisabledItemsComboBox() {
		super();
		super.setRenderer(new DisabledItemsRenderer());
	}

	private Set disabled_items = new HashSet();
	private boolean disabledItemType = false;

	public void setDisabledItemType(boolean b) {
		disabledItemType = b;

		if (disabledItemType) {
			addDisabledItem("Attribute Annotation");
			addDisabledItem("Attribute Annotation Parameter");

		}
	}

	public void addDisabledItem(String item) {
		for (int i = 0; i < this.getItemCount(); i++) {
			if (this.getItemAt(i).equals(item))
				disabled_items.add(i);
		}
		// if (this.getSelectedItem().equals(item)) {
		// showInformationDialog();
		// }

	}

	public void removeDisabledItem(String item) {
		for (int i = 0; i < this.getItemCount(); i++) {
			if (this.getItemAt(i).equals(item))
				disabled_items.remove(i);
		}
		if (disabledItemType) {
			addDisabledItem("Attribute Annotation");
			addDisabledItem("Attribute Annotation Parameter");

		}
	}

	public void addItem(String anObject, boolean disabled) {
		super.addItem(anObject);
		if (disabled) {
			disabled_items.add(getItemCount() - 1);
		}
	}

	@Override
	public void removeAllItems() {
		super.removeAllItems();
		disabled_items = new HashSet();
	}

	@Override
	public void removeItemAt(final int anIndex) {
		super.removeItemAt(anIndex);
		disabled_items.remove(anIndex);
	}

	@Override
	public void removeItem(final Object anObject) {
		for (int i = 0; i < getItemCount(); i++) {
			if (getItemAt(i) == anObject) {
				disabled_items.remove(i);
			}
		}
		super.removeItem(anObject);
	}

	@Override
	public void setSelectedIndex(int index) {
		if (!disabled_items.contains(index)) {
			super.setSelectedIndex(index);
		}
	}

	public void setNotDisabledItem() {
		for (int i = 0; i < this.getItemCount(); i++) {
			if (!disabled_items.contains(i)) {
				setSelectedIndex(i);
				return;
			}
		}
	}

	private class DisabledItemsRenderer extends BasicComboBoxRenderer {

		@Override
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {

			if (isSelected) {
				setIcon(null);
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setIcon(null);
				setBackground(list.getBackground());
				setForeground(list.getForeground());
				setToolTipText("");
			}
			if (disabled_items.contains(index)) {
				setBackground(list.getBackground());
				if (disabledItemType) {
					setToolTipText("Attribute Annotation not possible because of not primitive attribute type");
					setForeground(UIManager.getColor("TextField.inactiveBackground"));
					final java.net.URL imgURL = IntegrationMechanismCodeGenerator.class.getResource("/dialogError.png");
					final ImageIcon ic = new ImageIcon(imgURL);
					setIcon(ic);
				} else {
					setToolTipText("The parent Object have to use the Type Annotation mechanism");
					setForeground(UIManager.getColor("Label.disabledForeground"));
					final java.net.URL imgURL = IntegrationMechanismCodeGenerator.class
							.getResource("/dialogWarning.png");
					final ImageIcon ic = new ImageIcon(imgURL);
					setIcon(ic);
				}

			}
			setFont(list.getFont());

			final Mechanism mechanism = (Mechanism) value;
			setText((value == null) ? "" : mechanism.getName());
			return this;
		}
	}

}
