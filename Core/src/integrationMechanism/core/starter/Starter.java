package integrationMechanism.core.starter;

import java.awt.EventQueue;

import integrationMechanism.core.generator.IntegrationMechanismCodeGenerator;

public class Starter {

	public static void main(String[] args) {
		
		
		
		// TODO Auto-generated method stub
		IntegrationMechanismCodeGenerator myTest = new IntegrationMechanismCodeGenerator();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FileSelectionGUI frame = new FileSelectionGUI(myTest);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
