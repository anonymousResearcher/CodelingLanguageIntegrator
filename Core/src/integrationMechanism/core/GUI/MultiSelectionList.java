package integrationMechanism.core.GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.modelversioning.emfprofile.EMFProfilePackage;
import org.modelversioning.emfprofile.Profile;
import org.modelversioning.emfprofileapplication.EMFProfileApplicationPackage;

public class MultiSelectionList extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private ArrayList<File> profileFiles;
	private JFrame caller;
	private HashMap<String, Resource> ressourceMap;
	DefaultListModel<String> model = new DefaultListModel<>();
	private JList<String> list;
	private int width;
	private int heigth;
	private JScrollPane scrollPane;
	private JButton okButton;
	/**
	 * Create the dialog.
	 */
	public MultiSelectionList() {
		profileFiles = new ArrayList<File>();
		ressourceMap = new HashMap<String, Resource>();
		setBounds(100, 100, 330, 306);
		
		
		
		addComponentListener(new ComponentAdapter() {
		

			public void componentResized(ComponentEvent e) {
				width = e.getComponent().getWidth();
				heigth = e.getComponent().getHeight();
				textField.setBounds(10 , heigth - 90, width-35, 20);
				okButton.setBounds(10, heigth-65, width - 35, 23);
				scrollPane.setBounds(10, 10, width - 35 , heigth - 100);
				list.setBounds(10, 10, width - 35, heigth - 100);

			}
		});
		
		
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 320, 215);
		contentPanel.add(scrollPane);

		list = new JList<String>(model);
		scrollPane.setViewportView(list);

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setBounds(0, 219, 320, 20);
		textField.setText("Select filepath");
		textField.addMouseListener(saveFolderChooseListener);
		contentPanel.add(textField);
		textField.setColumns(10);
		okButton = new JButton("OK");
		okButton.setBounds(0, 240, 314, 23);
		contentPanel.add(okButton);
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				caller.setVisible(true);
				closeWindow();

			}
		});
		okButton.setActionCommand("OK");
		getRootPane().setDefaultButton(okButton);

	}

	MouseAdapter saveFolderChooseListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = chooser.showOpenDialog(contentPanel);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
				addFiles(chooser.getSelectedFile());
				for (File f : profileFiles) {
					Resource res = getProfile(f.getAbsolutePath());
					if (!ressourceMap.containsKey(((Profile) res.getContents().get(0)).getName()))
						ressourceMap.put(((Profile) res.getContents().get(0)).getName(), res);
				}
				fillList();
				textField.setText(chooser.getSelectedFile().getPath());
			}
		}
	};

	private void addFiles(File folder) {
		for (File x : folder.listFiles()) {
			if (x.isDirectory())
				addFiles(x);
			else {
				if (x.getName().contains("."))
					if (x.getName().substring(x.getName().lastIndexOf(".") + 1).equals("emfprofile_diagram"))
						profileFiles.add(x);
			}
		}
	}

	private void closeWindow() {
		this.dispose();
	}

	public JFrame getCaller() {
		return caller;
	}

	public void setCaller(JFrame caller) {
		this.caller = caller;
	}

	protected Resource getProfile(String ecoreFile) {
		EMFProfilePackage.eINSTANCE.eClass();
		EMFProfileApplicationPackage.eINSTANCE.eClass();
		NotationPackage.eINSTANCE.eClass();

		ResourceSet resSet = new ResourceSetImpl();
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("emfprofile_diagram",
				new EcoreResourceFactoryImpl());

		Resource resource = resSet.getResource(URI.createFileURI(ecoreFile), true);
		return resource;
	}

	private void fillList() {
		LinkedList<String> profileList = new LinkedList<String>(); 
		Iterator it = ressourceMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			profileList.add((String) pair.getKey());
		}
		
		java.util.Collections.sort(profileList);
		
		for(String s : profileList)
			model.addElement(s);
	}

	public Resource[] getSelectedResources() {
		Resource[] selectedResources = new Resource[list.getSelectedIndices().length];
		for (int i = 0; i < list.getSelectedIndices().length; i++) {
			selectedResources[i] = ressourceMap.get(list.getSelectedValuesList().get(i));
		}
		return selectedResources;
	}

}
