package integrationMechanism.core.generator;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import javax.swing.JTree;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import org.codeling.mechanisms.ClassMechanism;
import org.codeling.mechanisms.Mechanism;
import org.codeling.mechanisms.MechanismsMapping;
import org.codeling.mechanisms.classes.ContainmentOperationMechanism;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.internal.IMavenConstants;
import org.eclipse.m2e.core.project.IProjectConfigurationManager;
import org.eclipse.m2e.core.project.ResolverConfiguration;

import integrationMechanism.core.GUI.MechanismSelectionGUI;
import integrationMechansim.core.utils.Utils;

public class IntegrationMechanismCodeGenerator {
	public static final String PACKAGE_PREFIX = "org.codeling.lang";
	private String ecoreFile;
	public String modelName;
	IProject metaModelProject = null;

	/**
	 * A map of {@link EClass}es, {@link EAttribute}s, and {@link EReference}s
	 * to mechanisms for translation.
	 */
	private HashMap<ENamedElement, Mechanism> targetMap;

	public void loadModel(String ecoreFile) {
		this.ecoreFile = ecoreFile;
		modelName = ecoreFile.substring(ecoreFile.lastIndexOf(File.separator) + 1, ecoreFile.lastIndexOf("."));
		final Resource ecoreModel = loadEcore();

		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final String projectName = ecoreModel.getURI().toPlatformString(true).split("/")[1];
		metaModelProject = root.getProject(projectName);

		startSelectionGUI(ecoreModel, this);
	}

	public void startSelectionGUI(Resource model, IntegrationMechanismCodeGenerator test) {

		EventQueue.invokeLater(() -> {
			try {
				final MechanismSelectionGUI frame = new MechanismSelectionGUI(model, test);
				frame.setVisible(true);
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
	}

	public void generateCode(JTree tree, boolean inheritance) throws JavaModelException, CoreException {
		targetMap = new HashMap<>();
		final MutableTreeNode rootNode = (MutableTreeNode) tree.getModel().getRoot();
		try {
			generateMechanisms(rootNode);
		} catch (URISyntaxException | IOException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fillTargetMap(rootNode);
	}

	public Resource loadEcore() {
		final ResourceSet resSet = new ResourceSetImpl();
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
		final Resource resource = resSet.getResource(URI.createPlatformResourceURI(ecoreFile, true), true);
		return resource;

	}

	public void generateMechanisms(TreeNode node) throws URISyntaxException, IOException, InstantiationException,
			IllegalAccessException, JavaModelException, CoreException {

		final IPackageFragmentRoot mm_srcFolder = createProject(modelName + ".mm");
		final IPackageFragmentRoot transformation_srcFolder = createProject(modelName + ".transformation");
		final IPackageFragmentRoot runtime_srcFolder = createProject(modelName + ".runtime");
		final String metaModelProjectName = metaModelProject.getName();
		final String metaModelProjectVersion = readProjectVersion(metaModelProject);

		final HashMap<ENamedElement, IJavaElement> modelToCodeMap = new HashMap<>();
		generateMechanisms_breadthFirst(node, mm_srcFolder, transformation_srcFolder, runtime_srcFolder,
				modelToCodeMap);

		createFileInProject(mm_srcFolder.getJavaProject().getProject(), "pom.xml",
				String.format(Utils.readFile("/Maven/POMTemplateMM"), modelName + ".mm", modelName + ".mm"));
		IFolder metaInf = mm_srcFolder.getJavaProject().getProject().getFolder("META-INF");
		if (!metaInf.exists())
			metaInf.create(true, true, null);
		createFileInProject(mm_srcFolder.getJavaProject().getProject(), "META-INF/MANIFEST.MF", String.format(
				Utils.readFile("/Maven/MANIFESTMM"), modelName + ".mm", PACKAGE_PREFIX + "." + modelName + ".mm"));
		mm_srcFolder.getJavaProject().getProject().refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		createFileInProject(mm_srcFolder.getJavaProject().getProject(), "build.properties",
				Utils.readFile("/Maven/build.properties.template"));
		enableMavenNature(mm_srcFolder.getJavaProject().getProject());

		createFileInProject(runtime_srcFolder.getJavaProject().getProject(), "pom.xml",
				String.format(Utils.readFile("/Maven/POMTemplateRuntime"), modelName + ".runtime",
						modelName + ".runtime", modelName + ".mm"));
		metaInf = runtime_srcFolder.getJavaProject().getProject().getFolder("META-INF");
		if (!metaInf.exists())
			metaInf.create(true, true, null);
		createFileInProject(runtime_srcFolder.getJavaProject().getProject(), "META-INF/MANIFEST.MF", String
				.format(Utils.readFile("/Maven/MANIFESTRuntime"), modelName + ".runtime", modelName + ".runtime"));
		runtime_srcFolder.getJavaProject().getProject().refreshLocal(IResource.DEPTH_INFINITE,
				new NullProgressMonitor());
		createFileInProject(runtime_srcFolder.getJavaProject().getProject(), "build.properties",
				Utils.readFile("/Maven/build.properties.template"));
		enableMavenNature(runtime_srcFolder.getJavaProject().getProject());

		metaInf = transformation_srcFolder.getJavaProject().getProject().getFolder("META-INF");
		if (!metaInf.exists())
			metaInf.create(true, true, null);
		createFileInProject(transformation_srcFolder.getJavaProject().getProject(), "META-INF/MANIFEST.MF",
				String.format(Utils.readFile("/Maven/MANIFESTTransformation"), modelName + ".transformation",
						metaModelProjectName, metaModelProjectVersion));
		createFileInProject(transformation_srcFolder.getJavaProject().getProject(), "pom.xml",
				String.format(Utils.readFile("/Maven/POMTemplateTransformation"), modelName + ".transformation",
						modelName + ".transformation"));
		createFileInProject(transformation_srcFolder.getJavaProject().getProject(), "build.properties",
				Utils.readFile("/Maven/build.properties.template"));
		enableMavenNature(transformation_srcFolder.getJavaProject().getProject());

		transformation_srcFolder.getJavaProject().getProject().refreshLocal(IResource.DEPTH_INFINITE,
				new NullProgressMonitor());
	}

	Queue<TreeNode> queue = new LinkedList<TreeNode>();

	private void generateMechanisms_breadthFirst(TreeNode root, final IPackageFragmentRoot mm_srcFolder,
			IPackageFragmentRoot transformation_srcFolder, IPackageFragmentRoot runtime_srcFolder,
			Map<ENamedElement, IJavaElement> modelToCodeMap) throws JavaModelException, URISyntaxException, IOException,
			InstantiationException, IllegalAccessException, CoreException {
		if (root == null)
			return;
		queue.clear();
		queue.add(root);
		while (!queue.isEmpty()) {
			final TreeNode node = queue.remove();

			// Process node
			generateMechanisms(node, mm_srcFolder, transformation_srcFolder, runtime_srcFolder, modelToCodeMap);

			if (node.getAllowsChildren()) {
				for (int i = 0; i < node.getChildCount(); i++) {
					queue.add(node.getChildAt(i));
				}
			}
		}
	}

	public void fillMechanismsMap(JTree tree) {
		final MutableTreeNode rootNode = (MutableTreeNode) tree.getModel().getRoot();

		if (rootNode == null)
			return;
		queue.clear();
		queue.add(rootNode);
		while (!queue.isEmpty()) {
			final TreeNode node = queue.remove();

			// Process node
			if (node instanceof EObjectTreeNode) {
				final EObjectTreeNode eNode = (EObjectTreeNode) node;
				final ENamedElement heldObject = (ENamedElement) eNode.getObjectToHold();
				final Mechanism mechanism = eNode.getTargetMechanism();
				MechanismsMapping.getInstance().put(heldObject, mechanism.getClass());
			}

			if (node.getAllowsChildren()) {
				for (int i = 0; i < node.getChildCount(); i++) {
					queue.add(node.getChildAt(i));
				}
			}
		}
	}

	private void generateMechanisms(TreeNode node, final IPackageFragmentRoot mm_srcFolder,
			IPackageFragmentRoot transformation_srcFolder, IPackageFragmentRoot runtime_srcFolder,
			Map<ENamedElement, IJavaElement> modelToCodeMap) throws JavaModelException, URISyntaxException, IOException,
			InstantiationException, IllegalAccessException, CoreException {

		if (node instanceof EObjectTreeNode) {
			final EObjectTreeNode eNode = (EObjectTreeNode) node;
			final ENamedElement heldObject = (ENamedElement) eNode.getObjectToHold();
			final Mechanism mechanism = eNode.getTargetMechanism();
			MechanismsMapping.getInstance().put(heldObject, mechanism.getClass());

			if (mechanism instanceof ClassMechanism) {
				ENamedElement target = heldObject;
				if (mechanism instanceof ContainmentOperationMechanism)
					target = ((EReference) heldObject).getEType();
				// The containment operation mechanism is special, because it
				// represents a reference *and* the target
				((ClassMechanism) mechanism).setEClass((EClass) target);
			}

			mechanism.setModelToCodeMap(modelToCodeMap);
			mechanism.setLanguageName(modelName);

			// Do not handle the "name" and "id" attributes
			if (!(heldObject instanceof EAttribute && ("name".equals(((EAttribute) heldObject).getName())
					|| "id".equals(((EAttribute) heldObject).getName())))) {
				IPackageFragment fragment = createPackageFragment("mm", heldObject, mm_srcFolder);
				final IJavaElement mmCodeElement = mechanism.createMetaModelLibrary(fragment, heldObject);
				modelToCodeMap.put(heldObject, mmCodeElement);

				fragment = createPackageFragment("transformation", heldObject, transformation_srcFolder);
				mechanism.createTransformation(fragment, heldObject);

				fragment = createPackageFragment("runtime", heldObject, runtime_srcFolder);
				mechanism.createRuntime(fragment, heldObject);
			}
		}

	}

	private String readProjectVersion(IProject project) throws CoreException, IOException {
		final String bundleVersionKey = "Bundle-Version: ";
		final IFile manifest = project.getFile("META-INF/MANIFEST.MF");
		final BufferedReader reader = new BufferedReader(new InputStreamReader(manifest.getContents()));
		String line = "";
		while ((line = reader.readLine()) != null) {
			line = line.trim();
			if (line.startsWith(bundleVersionKey)) {
				return line.substring(bundleVersionKey.length());
			}
		}
		throw new IllegalStateException(
				"Could not read projct version. The project does not contain a line starting with 'Bundle-Version: '");
	}

	private void enableMavenNature(final IProject project) {
		// Enable Maven Nature for all projects
		final Job job = new Job("Enabling Maven Nature") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					final ResolverConfiguration configuration = new ResolverConfiguration();
					configuration.setResolveWorkspaceProjects(true);
					configuration.setSelectedProfiles(""); //$NON-NLS-1$

					final boolean hasMavenNature = project.hasNature(IMavenConstants.NATURE_ID);

					final IProjectConfigurationManager configurationManager = MavenPlugin
							.getProjectConfigurationManager();

					configurationManager.enableMavenNature(project, configuration, monitor);

					if (!hasMavenNature) {
						configurationManager.updateProjectConfiguration(project, monitor);
					}
				} catch (final CoreException ex) {
					ex.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}

	private void createFileInProject(IProject project, String fileName, String content)
			throws UnsupportedEncodingException, CoreException {
		final InputStream stream = new ByteArrayInputStream(content.getBytes("UTF-8"));
		final IFile file = project.getFile(fileName);
		if (file.exists())
			file.setContents(stream, true, false, null);
		else
			file.create(stream, true, null);
	}

	private IPackageFragment createPackageFragment(String suffix, final EObject heldObject,
			final IPackageFragmentRoot srcFolder) throws JavaModelException {
		// create package fragment
		String packageName = PACKAGE_PREFIX + "." + modelName + "." + suffix;
		if (heldObject instanceof EStructuralFeature) {
			final String containerName = ((ENamedElement) ((EStructuralFeature) heldObject).eContainer()).getName();
			packageName += "." + containerName.toLowerCase() + "_feature";
		}

		IPackageFragment fragment = srcFolder.getPackageFragment(packageName);
		if (!fragment.exists())
			fragment = srcFolder.createPackageFragment(packageName, true, null);
		return fragment;
	}

	private IPackageFragmentRoot createProject(String projectName) throws CoreException, JavaModelException {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final IProject project = root.getProject(projectName);
		if (!project.exists())
			project.create(null);
		project.open(null);

		// set the Java nature
		final IProjectDescription description = project.getDescription();
		description.setNatureIds(new String[] { JavaCore.NATURE_ID });

		// create the project
		project.setDescription(description, null);
		final IJavaProject javaProject = JavaCore.create(project);

		// create folder by using resources package
		IFolder folder = project.getFolder("src");
		if (!folder.exists())
			folder.create(true, true, null);
		folder = folder.getFolder("main");
		if (!folder.exists())
			folder.create(true, true, null);
		final IFolder sourceFolder = folder.getFolder("java");
		if (!sourceFolder.exists())
			sourceFolder.create(true, true, null);

		// set the build path
		final IClasspathEntry[] buildPath = { JavaCore.newSourceEntry(project.getFullPath().append("src/main/java")),
				JavaRuntime.getDefaultJREContainerEntry() };

		folder = project.getFolder("target");
		if (!folder.exists())
			folder.create(true, true, null);
		folder = folder.getFolder("classes");
		if (!folder.exists())
			folder.create(true, true, null);
		javaProject.setRawClasspath(buildPath, project.getFullPath().append("target/classes"), null);

		// Add folder to Java element
		final IPackageFragmentRoot pfRoot = javaProject.getPackageFragmentRoot(sourceFolder);
		return pfRoot;
	}

	private void fillTargetMap(TreeNode key) {
		if (key instanceof EObjectTreeNode) {
			final EObjectTreeNode eKey = (EObjectTreeNode) key;
			final ENamedElement eElement = (ENamedElement) eKey.getObjectToHold();
			targetMap.put(eElement, eKey.getTargetMechanism());
		}

		for (int i = 0; i < key.getChildCount(); i++)
			fillTargetMap(key.getChildAt(i));

	}

}
