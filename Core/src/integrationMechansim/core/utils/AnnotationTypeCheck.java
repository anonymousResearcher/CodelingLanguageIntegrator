package integrationMechansim.core.utils;

import java.util.Arrays;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EcorePackage;

public class AnnotationTypeCheck {
	private static EDataType[] allowedDatatypes = {
			EcorePackage.eINSTANCE.getEShort(),
			EcorePackage.eINSTANCE.getELong(),
			EcorePackage.eINSTANCE.getEInt(),
			EcorePackage.eINSTANCE.getEByte(),
			EcorePackage.eINSTANCE.getEFloat(),
			EcorePackage.eINSTANCE.getEDouble(),
			EcorePackage.eINSTANCE.getEBoolean(),
			EcorePackage.eINSTANCE.getEString(),
			EcorePackage.eINSTANCE.getEEnumerator(),
			EcorePackage.eINSTANCE.getEJavaClass()			
	};
	


  public static boolean isAllowedDatatype(EDataType e){
	 return Arrays.asList(allowedDatatypes).contains(e);
  }
	
	

}
