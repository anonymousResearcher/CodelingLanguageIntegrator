package integrationMechanism.core.GUI

import org.codeling.mechanisms.AttributeMechanism
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment

class GenericAttributeMechanism extends AttributeMechanism {

	override getName() {
		return "Custom Translation";
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EAttribute eAttribute = element as EAttribute;
		val String eAttributeInstantiation = eAttribute.EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.AnnotatedMemberReferenceTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import org.codeling.mechanisms.transformation.AttributeMechanismTransformation;
			import org.codeling.utils.CodelingException;
			import «eAttribute.packageName».«eAttribute.packageName»Package;
			import «eAttribute.packageName».«eAttribute.EContainingClass.name»;
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			
			public class «typeName»Transformation
					extends AttributeMechanismTransformation<«eAttribute.EContainingClass.name», «eAttribute.EType.name»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
					super(parentTransformation, «eAttributeInstantiation»);
				}
			
				@Override
				public void createCodeFragments() throws CodelingException {
					// TODO Auto-generated method stub
					
				}
			
				@Override
				public void updateCodeFragments() throws CodelingException {
					// TODO Auto-generated method stub
					
				}
			
				@Override
				public void deleteCodeFragments() throws CodelingException {
					// TODO Auto-generated method stub
					
				}
			
				@Override
				public «eAttribute.EContainingClass.name» transformToModel() throws CodelingException {
					// TODO Auto-generated method stub
					return null;
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return true;
	}

}
