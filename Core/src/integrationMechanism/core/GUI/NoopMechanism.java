package integrationMechanism.core.GUI;

import org.codeling.mechanisms.Mechanism;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;

public class NoopMechanism extends Mechanism {
	@Override
	public String getName() {
		return "Do Not Translate";
	}

	@Override
	public IJavaElement createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		return null;
	}

	@Override
	public boolean canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return true;
	}
}
