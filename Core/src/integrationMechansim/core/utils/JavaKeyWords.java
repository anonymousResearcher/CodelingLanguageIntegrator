package integrationMechansim.core.utils;

import java.util.Arrays;

public class JavaKeyWords {
	
	private static String[] keywords = {"abstract", "continue", "for", "new", "switch", "assert", "default", "goto", "package", "synchronized",
	"boolean", "do", "if", "private", "this",
	"break", "double", "implements", "protected", "throw",
	"byte", "else", "import", "public", "throws",
	"case", "enum", "instanceof", "return", "transient",
	"catch", "extends", "int", "short", "try",
	"char", "final" ,"interface" ,"static" ,"void",
	"class" ,"finally" ,"long" ,"strictfp" ,"volatile",
	"const" ,"float" ,"native" ,"super" ,"while"};
	
	
	public static boolean isKeyword(String x){
		return Arrays.asList(keywords).contains(x.toLowerCase());
	}
	
	
	public static String checkForKeyword(String x){
		if(isKeyword(x))
			return "_" + x;
		else return x;
	}
	

}
