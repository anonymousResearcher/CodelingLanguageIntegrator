package integrationMechanism.core.generator;

import javax.swing.tree.DefaultMutableTreeNode;

import org.codeling.mechanisms.Mechanism;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * A container that holds an {@link EObject} with meta data whether it is an
 * {@link EClass}, {@link EAttribute}, {@link org.eclipse.emf.ecore.EReference},
 * or {@link org.eclipse.emf.ecore.EReference} with containment feature, and the
 * target mechanism.
 *
 */
public class EObjectTreeNode extends DefaultMutableTreeNode {
	private EObject objectToHold;
	private int nodeEtype;
	private Mechanism targetMechanism;
	public static int EClass = 0;
	public static int EAttribute = 1;
	public static int EReference = 2;
	public static int EContainment = 3;

	public EObjectTreeNode(String x) {
		super(x);
	}

	public EObject getObjectToHold() {
		return objectToHold;
	}

	public void setObjectToHold(EObject objectToHold) {
		this.objectToHold = objectToHold;
	}

	public int getType() {
		return nodeEtype;
	}

	public void setType(int type) {
		this.nodeEtype = type;
	}

	public Mechanism getTargetMechanism() {
		return targetMechanism;
	}

	public void setTargetMechanism(Mechanism targetMechanism) {
		this.targetMechanism = targetMechanism;
	}
}
