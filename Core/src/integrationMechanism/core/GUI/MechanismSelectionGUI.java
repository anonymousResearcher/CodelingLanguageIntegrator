package integrationMechanism.core.GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.codeling.mechanisms.Mechanism;
import org.codeling.mechanisms.Mechanisms;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.emfprofile.Profile;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import integrationMechanism.core.generator.EObjectTreeNode;
import integrationMechanism.core.generator.IntegrationMechanismCodeGenerator;
import integrationMechansim.core.utils.Utils;

public class MechanismSelectionGUI extends JFrame {
	private JPanel contentPane;
	private ArrayList<EClass> classes;
	private JTree tree;
	private JScrollPane scrollPane;
	private final IntegrationMechanismCodeGenerator generatorClass;
	private JCheckBox chkInheritance;
	private final Resource ecoreResource;
	JLabel lblInheritance;
	private final HashMap<EObjectTreeNode, DisabledItemsComboBox> treeToCombo = new HashMap<>();
	private int width = 600;
	private int heigth = 800;
	private final int minwidth = 500;
	private final int minheigth = 400;
	private final JFrame thisFrame = this;
	DefaultMutableTreeNode root;
	DefaultMutableTreeNode invisibleRoot;
	JList<String> list;
	DefaultListModel<String> model;
	JScrollPane listpane;
	String deselect;
	boolean output = false;
	private String changeMessage;

	// Buttons
	private JButton btnGenerate;
	private JButton btnProfiles;
	private JButton btnLoadSettings;
	private JButton btnSaveSettings;

	public MechanismSelectionGUI(Resource ecore, IntegrationMechanismCodeGenerator test) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (final Exception e) {
			e.printStackTrace();
		}

		generatorClass = test;
		ecoreResource = ecore;
		createPane();
		createButtonElement();
		createCheckBox();
		createErrorList();
		createScrollPane();
		createJTRee();
		addListener();

	}

	// ----------------------GUI Elemente-------------------//

	private void createPane() {
		setResizable(true);
		setTitle("Codeling Language Integrator");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, width, heigth);
		setMinimumSize(new Dimension(minwidth, minheigth));
		addComponentListener(paneAdapter);

		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("CheckBox.light"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}

	private void createErrorList() {
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		listpane = new JScrollPane(list);
		contentPane.add(listpane);
	}

	private void createButtonElement() {
		btnGenerate = new JButton("Generate Classes");
		btnGenerate.setBackground(Color.WHITE);
		btnGenerate.addMouseListener(generateClassesListener);
		contentPane.add(btnGenerate);

		btnProfiles = new JButton("Select Profiles");
		btnProfiles.setBackground(Color.WHITE);
		btnProfiles.addActionListener(selectProfilesListener);
		contentPane.add(btnProfiles);

		btnSaveSettings = new JButton("Save selection");
		btnSaveSettings.setBackground(Color.WHITE);
		btnSaveSettings.addMouseListener(saveSelectionListener);
		contentPane.add(btnSaveSettings);

		btnLoadSettings = new JButton("Load selection");
		btnLoadSettings.setBackground(Color.WHITE);
		btnLoadSettings.addMouseListener(loadSelectionListener);
		contentPane.add(btnLoadSettings);

	}

	private void createCheckBox() {
		chkInheritance = new JCheckBox();
		chkInheritance.setEnabled(true);
		chkInheritance.setSelected(true);
		chkInheritance.setBounds(10, 405, 25, 25);
		contentPane.add(chkInheritance);

		lblInheritance = new JLabel();
		lblInheritance.setText("create inherited attributes");
		lblInheritance.setBounds(40, 405, 200, 25);
		contentPane.add(lblInheritance);
	}

	private void createScrollPane() {
		scrollPane = new JScrollPane();
		scrollPane.getViewport().addChangeListener(

				e -> {
					hideSelectionBoxes();
					addSelectionBoxes();
				});

		scrollPane.setBounds(10, 10, width - 260, heigth - 100);
		contentPane.add(scrollPane);

	}

	private void createJTRee() {
		invisibleRoot = new DefaultMutableTreeNode("RootNode");
		root = new DefaultMutableTreeNode(generatorClass.modelName);
		EObjectTreeNode leaf;
		classes = new ArrayList<EClass>();

		final List<EObject> rootPackages = ecoreResource.getContents();
		for (final EObject e : rootPackages) {
			getClasses(e);
		}

		for (final EClass x : classes) {
			leaf = new EObjectTreeNode(x.getName());
			leaf.setObjectToHold(x);
			leaf.setType(EObjectTreeNode.EClass);
			root.add(leaf);

			final DefaultMutableTreeNode parameterNode = new DefaultMutableTreeNode("Attributes");
			for (final EAttribute param : x.getEAllAttributes()) {
				if (!param.getName().equals("id")) {
					final EObjectTreeNode paramTemp = new EObjectTreeNode(param.getName());
					paramTemp.setObjectToHold(param);
					paramTemp.setType(EObjectTreeNode.EAttribute);
					parameterNode.add(paramTemp);
				}
			}
			leaf.add(parameterNode);

			final DefaultMutableTreeNode referenceNode = new DefaultMutableTreeNode("References");
			for (final EReference ref : x.getEReferences()) {
				if (!x.getEAllContainments().contains(ref)) {
					final EObjectTreeNode paramTemp = new EObjectTreeNode(ref.getName());
					paramTemp.setObjectToHold(ref);
					paramTemp.setType(EObjectTreeNode.EReference);
					referenceNode.add(paramTemp);
				}
			}
			leaf.add(referenceNode);

			final DefaultMutableTreeNode containmentNode = new DefaultMutableTreeNode("Containments");
			for (final EReference cont : x.getEAllContainments()) {
				final EObjectTreeNode paramTemp = new EObjectTreeNode(cont.getName());
				paramTemp.setObjectToHold(cont);
				paramTemp.setType(EObjectTreeNode.EContainment);
				containmentNode.add(paramTemp);
			}
			leaf.add(containmentNode);

		}
		invisibleRoot.add(root);

		final DefaultTreeModel model = new DefaultTreeModel(invisibleRoot);
		tree = new JTree(model);
		tree.expandRow(1);
		tree.setRootVisible(false);
		scrollPane.setViewportView(tree);
		tree.setVisibleRowCount(10);
		tree.setBorder(new LineBorder(new Color(0, 0, 0)));

		tree.addTreeWillExpandListener(new TreeWillExpandListener() {
			@Override
			public void treeWillCollapse(TreeExpansionEvent event) {
				hideSelectionBoxes();
			}

			@Override
			public void treeWillExpand(TreeExpansionEvent event) {
			}
		});
		tree.addTreeExpansionListener(new TreeExpansionListener() {
			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {
				addSelectionBoxes();
			}

			@Override
			public void treeExpanded(TreeExpansionEvent arg0) {
				addSelectionBoxes();
			}
		});
		addSelectionBoxes();

	}

	/**
	 * Adds boxes to select the mechanism for each Ecore Element in the JTree
	 */
	private void addSelectionBoxes() {
		for (int i = 0; i < tree.getRowCount(); i++) {
			final Rectangle treeBounds = tree.getRowBounds(i);
			final TreePath treePath = tree.getPathForLocation(treeBounds.x, treeBounds.y);
			final Object lastElement = treePath.getLastPathComponent();
			if (lastElement instanceof EObjectTreeNode) {
				if (!treeToCombo.containsKey(lastElement)) {
					final DisabledItemsComboBox comboBox = new DisabledItemsComboBox();
					comboBox.setFont(new Font("Tahoma", Font.PLAIN, 10));
					comboBox.setBounds(195, treeBounds.y + 12 - scrollPane.getVerticalScrollBar().getValue(), 200, 17);
					comboBox.setBackground(Color.WHITE);
					comboBox.setVisible(true);
					comboBox.setBorder(new LineBorder(Color.black));
					contentPane.add(comboBox);
					setMechanisms();
					treeToCombo.put((EObjectTreeNode) lastElement, comboBox);
					addValuesToComboBox((EObjectTreeNode) lastElement);
				} else {
					final DisabledItemsComboBox comboBox = treeToCombo.get(lastElement);
					comboBox.setBounds(width - 220, treeBounds.y + 12 - scrollPane.getVerticalScrollBar().getValue(),
							200, 17);
					if (comboBox.getY() + comboBox.getHeight() < scrollPane.getY() + scrollPane.getHeight()
							&& comboBox.getY() > scrollPane.getY())
						comboBox.setVisible(true);
					setMechanisms();
					treeToCombo.put((EObjectTreeNode) lastElement, comboBox);

				}
			}
		}
		addHiddenComboBoxes((TreeNode) tree.getModel().getRoot());
	}

	private void addHiddenComboBoxes(TreeNode node) {
		if (node instanceof EObjectTreeNode && !treeToCombo.containsKey(node)) {
			final DisabledItemsComboBox comboBox = new DisabledItemsComboBox();
			comboBox.setFont(new Font("Tahoma", Font.PLAIN, 10));
			comboBox.setBackground(Color.WHITE);
			comboBox.setVisible(false);
			comboBox.setBorder(new LineBorder(Color.black));
			contentPane.add(comboBox);
			setMechanisms();
			treeToCombo.put((EObjectTreeNode) node, comboBox);
			addValuesToComboBox((EObjectTreeNode) node);
		}
		for (int i = 0; i < node.getChildCount(); i++)
			addHiddenComboBoxes(node.getChildAt(i));
	}

	private void hideSelectionBoxes() {
		for (int i = 0; i < tree.getRowCount(); i++) {
			final Rectangle treeBounds = tree.getRowBounds(i);
			final TreePath path = tree.getPathForLocation(treeBounds.x, treeBounds.y);
			final Object lastElement = path.getLastPathComponent();
			if (lastElement instanceof EObjectTreeNode) {
				if (treeToCombo.containsKey(lastElement)) {
					final DisabledItemsComboBox comboBox = treeToCombo.get(lastElement);
					comboBox.setVisible(false);
				}
			}
		}
	}

	private void addValuesToComboBox(EObjectTreeNode node) {
		if (node.getType() == EObjectTreeNode.EClass) {
			treeToCombo.get(node).addItemListener(comboBoxChangeListener);
			for (final Mechanism mechanism : listOfClassMechanisms)
				treeToCombo.get(node).addItem(mechanism);
			if (((EClass) node.getObjectToHold()).isAbstract())
				treeToCombo.get(node).setSelectedItem(noopMechanism);
			else
				treeToCombo.get(node).setSelectedItem(genericClassMechanism);
		} else if (node.getType() == EObjectTreeNode.EAttribute) {
			for (final Mechanism mechanism : listOfAttributeMechanisms)
				treeToCombo.get(node).addItem(mechanism);
			if (((EClass) node.getObjectToHold().eContainer()).isAbstract()
					|| ((EAttribute) node.getObjectToHold()).getName().equals("name"))
				treeToCombo.get(node).setSelectedItem(noopMechanism);
			else
				treeToCombo.get(node).setSelectedItem(genericAttributeMechanism);
		} else if (node.getType() == EObjectTreeNode.EReference) {
			for (final Mechanism mechanism : listOfReferenceMechanisms)
				treeToCombo.get(node).addItem(mechanism);
			if (((EClass) node.getObjectToHold().eContainer()).isAbstract())
				treeToCombo.get(node).setSelectedItem(noopMechanism);
			else
				treeToCombo.get(node).setSelectedItem(genericReferenceMechanism);
		} else if (node.getType() == EObjectTreeNode.EContainment) {
			for (final Mechanism mechanism : listOfContainmentMechanisms)
				treeToCombo.get(node).addItem(mechanism);
			if (((EClass) node.getObjectToHold().eContainer()).isAbstract())
				treeToCombo.get(node).setSelectedItem(noopMechanism);
			else
				treeToCombo.get(node).setSelectedItem(genericReferenceMechanism);
		}

		// if (node.getType() == EObjectTreeNode.EClass)
		// for (final Mechanism m : ClassMechanism.values()) {
		// treeToCombo.get(node).addItemListener(comboBoxChangeListener);
		// treeToCombo.get(node).addItem(m);
		// }
		// if (node.getType() == EObjectTreeNode.EAttribute)
		// for (final Mechanism m : AttributeMechanism.values()) {
		// treeToCombo.get(node).addItem(m);
		// }
		// if (node.getType() == EObjectTreeNode.EReference)
		// for (final Mechanism m : ReferenceMechanism.values()) {
		// treeToCombo.get(node).addItem(m);
		// }
		// if (node.getType() == EObjectTreeNode.EContainment)
		// for (final Mechanism m : ContainmentMechanism.values()) {
		// treeToCombo.get(node).addItem(m);
		// }
		setComboBoxes(root);
		model.clear();

	}

	// ------------------------Ende der GUI
	// Elemente---------------------------//
	// ------------------------Listener &
	// Adapter------------------------------//

	private final ComponentAdapter paneAdapter = new ComponentAdapter() {
		@Override
		public void componentResized(ComponentEvent e) {
			width = e.getComponent().getWidth();
			heigth = e.getComponent().getHeight();
			hideSelectionBoxes();
			addSelectionBoxes();
			btnGenerate.setBounds(width - 140, heigth - 210, 120, 23);
			btnProfiles.setBounds(width - 140, heigth - 180, 120, 23);
			btnSaveSettings.setBounds(width - 260, heigth - 210, 120, 23);
			btnLoadSettings.setBounds(width - 260, heigth - 180, 120, 23);
			lblInheritance.setBounds(40, heigth - 210, 200, 25);
			chkInheritance.setBounds(10, heigth - 210, 25, 25);
			listpane.setBounds(10, heigth - 150, width - 30, 100);
			scrollPane.setBounds(10, 10, width - 260, heigth - 230);
		}
	};

	final Component me = this;
	private final MouseAdapter generateClassesListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			setMechanisms();
			model.addElement("Generating code...");
			try {
				generatorClass.fillMechanismsMap(tree);
				generatorClass.generateCode(tree, chkInheritance.isSelected());
			} catch (final CoreException e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(me, "Error while creating Code. Please see console output.",
						"Inane warning", JOptionPane.ERROR_MESSAGE);

			}
			model.addElement("Finished to generate code.");
		}
	};

	private final MouseAdapter saveSelectionListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			// Create a file chooser
			final JFileChooser fc = new JFileChooser();
			final FileNameExtensionFilter xmlfilter = new FileNameExtensionFilter("xml files (*.xml)", "xml");
			fc.setFileFilter(xmlfilter);
			// In response to a button click:
			fc.showSaveDialog(rootPane);
			File f = fc.getSelectedFile();
			if (!f.getAbsolutePath().contains(".xml"))
				f = new File(f.getAbsolutePath() + ".xml");
			final HashMap<String, String> mapping = new HashMap<>();
			for (final EObjectTreeNode key : treeToCombo.keySet()) {
				final String value = ((Mechanism) treeToCombo.get(key).getSelectedItem()).getName();
				if (key.getObjectToHold() instanceof EClass)
					mapping.put(((EClass) key.getObjectToHold()).getName(), value);
				else if (key.getObjectToHold() instanceof EAttribute)
					mapping.put(((EAttribute) key.getObjectToHold()).getEContainingClass().getName() + "."
							+ ((EAttribute) key.getObjectToHold()).getName(), value);
				else if (key.getObjectToHold() instanceof EReference)
					mapping.put(((EReference) key.getObjectToHold()).getEContainingClass().getName() + "."
							+ ((EReference) key.getObjectToHold()).getName(), value);
			}

			createMappingXML(f, mapping);
			model.addElement("Saved to file " + f.getAbsolutePath());
		}
	};

	private final MouseAdapter loadSelectionListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			final JFileChooser fc = new JFileChooser();
			final FileNameExtensionFilter xmlfilter = new FileNameExtensionFilter("xml files (*.xml)", "xml");
			fc.setFileFilter(xmlfilter);
			// In response to a button click:
			fc.showOpenDialog(rootPane);
			final File f = fc.getSelectedFile();
			final XStream xStream = new XStream(new DomDriver());

			final Map<String, String> extractedMap = (Map<String, String>) xStream.fromXML(f);
			outer: for (final String stringKey : extractedMap.keySet()) {
				for (final EObjectTreeNode key : treeToCombo.keySet()) {
					String name = null;
					if (key.getObjectToHold() instanceof EClass)
						name = ((EClass) key.getObjectToHold()).getName();
					else if (key.getObjectToHold() instanceof EAttribute)
						name = ((EAttribute) key.getObjectToHold()).getEContainingClass().getName() + "."
								+ ((EAttribute) key.getObjectToHold()).getName();
					else if (key.getObjectToHold() instanceof EReference)
						name = ((EReference) key.getObjectToHold()).getEContainingClass().getName() + "."
								+ ((EReference) key.getObjectToHold()).getName();
					if (name.equals(stringKey)) {
						treeToCombo.get(key).setSelectedItem(getMechanism(extractedMap.get(stringKey)));
						continue outer;
					}
				}
			}
			model.addElement("Loaded from file " + f.getAbsolutePath());
		}
	};

	private final ActionListener selectProfilesListener = e -> {
		final MultiSelectionList list = new MultiSelectionList();
		list.setCaller(thisFrame);
		thisFrame.setVisible(false);
		list.setModal(true);
		list.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		list.setVisible(true);
		addToTree(list.getSelectedResources());
	};

	private final ItemListener changeListener = e -> {
		if (output) {
			if (e.getStateChange() == ItemEvent.DESELECTED)
				deselect = ((Mechanism) e.getItem()).getName();
			else {
				String eObject = "null";
				String parent = "";
				for (final EObjectTreeNode key : treeToCombo.keySet()) {
					if (treeToCombo.get(key) == e.getSource()) {
						if (key.getType() == EObjectTreeNode.EClass) {
							eObject = ((EClass) key.getObjectToHold()).getName();
						} else if (key.getType() == EObjectTreeNode.EAttribute) {
							eObject = ((EAttribute) key.getObjectToHold()).getName();
							parent = ((EAttribute) key.getObjectToHold()).getEContainingClass().getName() + ".";
						} else if (key.getType() == EObjectTreeNode.EReference) {
							eObject = ((EReference) key.getObjectToHold()).getName();
							parent = ((EReference) key.getObjectToHold()).getEContainingClass().getName() + ".";
						} else if (key.getType() == EObjectTreeNode.EContainment) {
							eObject = ((EReference) key.getObjectToHold()).getName();
							parent = ((EReference) key.getObjectToHold()).getEContainingClass().getName() + ".";
						}
					}
				}
				if (model.getSize() == 0 || model.get(0) != changeMessage)
					model.add(0, changeMessage);
				model.add(1, "In " + parent + eObject + " wurde " + deselect + " durch " + e.getItem() + " ersetzt!");

			}
		}
	};

	ItemListener comboBoxChangeListener = e -> {

		if (e.getStateChange() == 1) {
			for (final EObjectTreeNode key : treeToCombo.keySet()) {
				if (treeToCombo.get(key) == e.getSource()) {
					String eObject = "";
					if (key.getType() == EObjectTreeNode.EClass)
						eObject = ((EClass) key.getObjectToHold()).getName();
					else if (key.getType() == EObjectTreeNode.EAttribute)
						eObject = ((EAttribute) key.getObjectToHold()).getName();
					else if (key.getType() == EObjectTreeNode.EReference)
						eObject = ((EReference) key.getObjectToHold()).getName();
					else if (key.getType() == EObjectTreeNode.EContainment)
						eObject = ((EReference) key.getObjectToHold()).getName();
					changeMessage = "Durch die Änderung von " + eObject + ":";
				}
			}
			setComboBoxes(root);
		}
	};

	private void addListener() {
		for (final EObjectTreeNode key : treeToCombo.keySet()) {

			treeToCombo.get(key).addItemListener(changeListener);

		}
	}

	private void setComboBoxes(TreeNode node) {
		output = true;
		final boolean changed = false;
		final DisabledItemsComboBox comboBox = treeToCombo.get(node);
		// if (comboBox != null) {
		//
		// if (node instanceof EObjectTreeNode) {
		// final EObjectTreeNode eNode = (EObjectTreeNode) node;
		// if (eNode.getObjectToHold() instanceof EClass) {
		// if (PackageHierarchyCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Package Hierarchy");
		// else {
		// comboBox.addDisabledItem("Package Hierarchy");
		// if (comboBox.getSelectedItem().equals("Package Hierarchy"))
		// changed = true;
		// }
		//
		// if (markerInterfaceCheck())
		// comboBox.removeDisabledItem("Marker Interface");
		// else {
		// comboBox.addDisabledItem("Marker Interface");
		// if (comboBox.getSelectedItem().equals("Marker Interface"))
		// changed = true;
		// }
		// if (typeAnnotationCheck())
		// comboBox.removeDisabledItem("Type Annotation");
		// else {
		// comboBox.addDisabledItem("Type Annotation");
		// if (comboBox.getSelectedItem().equals("Type Annotation"))
		// changed = true;
		// }
		// if (ninjaSingletonCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Ninja Singleton");
		// else {
		// comboBox.addDisabledItem("Ninja Singleton");
		// if (comboBox.getSelectedItem().equals("Ninja Singleton"))
		// changed = true;
		// }
		// if (staticInterfaceCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Static Interface");
		// else {
		// comboBox.addDisabledItem("Static Interface");
		// if (comboBox.getSelectedItem().equals("Static Interface"))
		// changed = true;
		// }
		//
		// }
		//
		// if (eNode.getObjectToHold() instanceof EAttribute) {
		// if (attributeAnnotationParameterCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Attribute Annotation Parameter");
		// else {
		// comboBox.addDisabledItem("Attribute Annotation Parameter");
		// if (comboBox.getSelectedItem().equals("Attribute Annotation Parameter"))
		// changed = true;
		// }
		// }
		//
		// if (eNode.getObjectToHold() instanceof EReference) {
		// if (markerInterfaceXto1Check((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Marker Interface X to 1");
		// else {
		// comboBox.addDisabledItem("Marker Interface X to 1");
		// if (comboBox.getSelectedItem().equals("Marker Interface X to 1"))
		// changed = true;
		// }
		// if (markerInterfaceXtoManyCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Marker Interface X to Many");
		// else {
		// comboBox.addDisabledItem("Marker Interface X to Many");
		// if (comboBox.getSelectedItem().equals("Marker Interface X to Many"))
		// changed = true;
		// }
		// if (typeAnnotationXto1Check((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Type Annotation X to 1");
		// else {
		// comboBox.addDisabledItem("Type Annotation X to 1");
		// if (comboBox.getSelectedItem().equals("Type Annotation X to 1"))
		// changed = true;
		// }
		// if (typeAnnotationXtoManyCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Type Annotation X to Many");
		// else {
		// comboBox.addDisabledItem("Type Annotation X to Many");
		// if (comboBox.getSelectedItem().equals("Type Annotation X to Many"))
		// changed = true;
		// }
		// if (staticInterfaceReferenceCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Static Interface Reference");
		// else {
		// comboBox.addDisabledItem("Static Interface Reference");
		// if (comboBox.getSelectedItem().equals("Static Interface Reference"))
		// changed = true;
		// }
		//
		// if (containmentOperationCheck((EObjectTreeNode) node))
		// comboBox.removeDisabledItem("Containment Operations");
		// else {
		// comboBox.addDisabledItem("Containment Operations");
		// if (comboBox.getSelectedItem().equals("Containment Operations"))
		// changed = true;
		// }
		//
		// if (parentOrReferenceNinjaSingletonCheck((EObjectTreeNode) node)) {
		// comboBox.addItem("Not translated");
		// comboBox.setSelectedItem("Not translated");
		// comboBox.setEnabled(false);
		// changed = false;
		// } else {
		// comboBox.removeItem("Not translated");
		// comboBox.setEnabled(true);
		// changed = true;
		// }
		// }
		//
		// }
		// if (changed)
		// comboBox.setNotDisabledItem();
		//
		// }
		for (int i = 0; i < node.getChildCount(); i++) {
			setComboBoxes(node.getChildAt(i));
		}
		output = false;
	}

	// ------------------------Hilfsmethoden/Checks------------------------//

	private boolean markerInterfaceCheck() {
		return true;
	}

	private boolean typeAnnotationCheck() {
		return true;
	}

	private boolean staticInterfaceCheck(EObjectTreeNode node) {
		if (((EClass) node.getObjectToHold()).getEAllReferences().size() > ((EClass) node.getObjectToHold())
				.getEAllContainments().size())
			return false;
		else
			return true;
	}

	private boolean ninjaSingletonCheck(EObjectTreeNode node) {

		final boolean check = ((EClass) node.getObjectToHold()).getEAllAttributes().size() > 1
				|| ((EClass) node.getObjectToHold()).getEAllReferences().size() > ((EClass) node.getObjectToHold())
						.getEAllContainments().size();
		if (check)
			return false;

		for (final EReference r : ((EClass) node.getObjectToHold()).getEAllContainments()) {
			final int refCount = getContainingCount(r.getEReferenceType());
			if (refCount > 1)
				return false;
		}

		for (final EAttribute a : ((EClass) node.getObjectToHold()).getEAttributes()) {
			if (!a.getName().toLowerCase().equals("id"))
				return false;
		}

		return true;
	}

	private boolean PackageHierarchyCheck(EObjectTreeNode node) {
		for (final EClass c : classes) {
			for (final EReference r : c.getEAllReferences()) {
				if (r.getEReferenceType() == node.getObjectToHold())
					return false;
			}
		}

		for (final EObjectTreeNode key : treeToCombo.keySet()) {
			if (key == node)
				continue;
			if (treeToCombo.get(key).getSelectedItem().equals("Package Hierarchy"))
				return false;
		}

		return true;
	}

	private boolean attributeAnnotationParameterCheck(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EAttribute) node.getObjectToHold()).getEContainingClass(), treeToCombo))
				.getSelectedItem().equals("Type Annotation")
				|| treeToCombo.get(Utils.getNodeForEObject(((EAttribute) node.getObjectToHold()).getEContainingClass(),
						treeToCombo)).getSelectedItem().equals("Static Interface"))
			return true;
		else
			return false;

	}

	private boolean attributeAnnotationCheck(EObjectTreeNode node) {
		if (!treeToCombo
				.get(Utils.getNodeForEObject(((EAttribute) node.getObjectToHold()).getEContainingClass(), treeToCombo))
				.getSelectedItem().equals("Static Interface"))
			return true;
		else
			return false;
	}

	private boolean constantMemberAttributeCheck(EObjectTreeNode node) {
		if (!treeToCombo
				.get(Utils.getNodeForEObject(((EAttribute) node.getObjectToHold()).getEContainingClass(), treeToCombo))
				.getSelectedItem().equals("Static Interface"))
			return true;
		else
			return false;
	}

	private boolean staticInterfaceReferenceCheck(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEReferenceType(), treeToCombo))
				.getSelectedItem().equals("Static Interface"))
			return true;
		else
			return false;

	}

	private boolean markerInterfaceXto1Check(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEReferenceType(), treeToCombo))
				.getSelectedItem().equals("Marker Interface"))
			return true;
		else
			return false;

	}

	private boolean markerInterfaceXtoManyCheck(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEReferenceType(), treeToCombo))
				.getSelectedItem().equals("Marker Interface"))
			return true;
		else
			return false;

	}

	private boolean typeAnnotationXto1Check(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEReferenceType(), treeToCombo))
				.getSelectedItem().equals("Type Annotation"))
			return true;
		else
			return false;

	}

	private boolean typeAnnotationXtoManyCheck(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEReferenceType(), treeToCombo))
				.getSelectedItem().equals("Type Annotation"))
			return true;
		else
			return false;

	}

	private boolean parentOrReferenceNinjaSingletonCheck(EObjectTreeNode node) {
		if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEContainingClass(), treeToCombo))
				.getSelectedItem().equals("Ninja Singleton"))
			return true;
		else if (treeToCombo
				.get(Utils.getNodeForEObject(((EReference) node.getObjectToHold()).getEReferenceType(), treeToCombo))
				.getSelectedItem().equals("Ninja Singleton"))
			return true;
		else
			return false;
	}

	public boolean containmentOperationCheck(EObjectTreeNode node) {

		final EClass e = ((EReference) node.getObjectToHold()).getEReferenceType();
		final EClass containing = ((EReference) node.getObjectToHold()).getEContainingClass();
		for (final EClass target : classes) {
			if (target == containing)
				continue;
			for (final EReference ref : target.getEAllReferences()) {
				if (ref.getEReferenceType() == e)
					return false;
			}
			for (final EReference ref : target.getEAllContainments()) {
				if (ref.getEReferenceType() == e)
					return false;
			}
		}
		return true;
	}

	private int getContainingCount(EClass search) {
		int i = 0;

		for (final EClass clazz : classes) {
			if (clazz.getEAllContainments().contains(search))
				i++;
		}
		return i;
	}

	private void getClasses(EObject e) {
		if (e instanceof EClass)
			classes.add((EClass) e);
		for (final EObject ecl : e.eContents())
			getClasses(ecl);
	}

	private void setMechanisms() {
		final Iterator it = treeToCombo.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry pair = (Map.Entry) it.next();
			final EObjectTreeNode key = (EObjectTreeNode) pair.getKey();
			final DisabledItemsComboBox value = (DisabledItemsComboBox) pair.getValue();
			key.setTargetMechanism((Mechanism) value.getSelectedItem());
		}
	}

	private void createMappingXML(File f, Map targetMap) {
		try {
			f.createNewFile();
		} catch (final IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		FileWriter fw;
		try {
			fw = new FileWriter(f);

			final XStream xStream = new XStream(new DomDriver());
			xStream.alias("map", java.util.Map.class);
			final String xml = xStream.toXML(targetMap);
			fw.write(xml);
			fw.flush();
			fw.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// -----EMF-Profies------//

	public void addToTree(Resource[] r) {
		EObjectTreeNode leaf;
		DefaultMutableTreeNode profNode;
		for (final Resource res : r) {

			profNode = new DefaultMutableTreeNode(((Profile) res.getContents().get(0)).getName());
			((DefaultTreeModel) tree.getModel()).insertNodeInto(profNode, invisibleRoot, invisibleRoot.getChildCount());

			final List<EObject> rootPackages = res.getContents();
			classes = new ArrayList<EClass>();
			for (final EObject e : rootPackages) {
				getClasses(e);
			}

			for (final EClass x : classes) {
				leaf = new EObjectTreeNode(x.getName());
				leaf.setObjectToHold(x);
				leaf.setType(EObjectTreeNode.EClass);
				((DefaultTreeModel) tree.getModel()).insertNodeInto(leaf, profNode, profNode.getChildCount());

				final DefaultMutableTreeNode parameterNode = new DefaultMutableTreeNode("Attributes");
				for (final EAttribute param : x.getEAllAttributes()) {
					final EObjectTreeNode paramTemp = new EObjectTreeNode(param.getName());
					paramTemp.setObjectToHold(param);
					paramTemp.setType(EObjectTreeNode.EAttribute);
					parameterNode.add(paramTemp);
				}
				((DefaultTreeModel) tree.getModel()).insertNodeInto(parameterNode, leaf, leaf.getChildCount());

				final DefaultMutableTreeNode referenceNode = new DefaultMutableTreeNode("References");

				for (final EReference ref : x.getEReferences()) {

					if (!x.getEAllContainments().contains(ref)) {
						final EObjectTreeNode paramTemp = new EObjectTreeNode(ref.getName());
						paramTemp.setObjectToHold(ref);
						paramTemp.setType(EObjectTreeNode.EReference);
						referenceNode.add(paramTemp);
					}
				}
				((DefaultTreeModel) tree.getModel()).insertNodeInto(referenceNode, leaf, leaf.getChildCount());

				final DefaultMutableTreeNode containmentNode = new DefaultMutableTreeNode("Containments");
				for (final EReference cont : x.getEAllContainments()) {
					final EObjectTreeNode paramTemp = new EObjectTreeNode(cont.getName());
					paramTemp.setObjectToHold(cont);
					paramTemp.setType(EObjectTreeNode.EContainment);
					containmentNode.add(paramTemp);
				}
				((DefaultTreeModel) tree.getModel()).insertNodeInto(containmentNode, leaf, leaf.getChildCount());

			}
			tree.expandPath(new TreePath(profNode.getPath()));

		}
		addSelectionBoxes();

	}

	LinkedList<Mechanism> listOfClassMechanisms = new LinkedList<Mechanism>();
	LinkedList<Mechanism> listOfAttributeMechanisms = new LinkedList<Mechanism>();
	LinkedList<Mechanism> listOfReferenceMechanisms = new LinkedList<Mechanism>();
	LinkedList<Mechanism> listOfContainmentMechanisms = new LinkedList<Mechanism>();
	GenericClassMechanism genericClassMechanism = new GenericClassMechanism();
	GenericAttributeMechanism genericAttributeMechanism = new GenericAttributeMechanism();
	GenericReferenceMechanism genericReferenceMechanism = new GenericReferenceMechanism();
	NoopMechanism noopMechanism = new NoopMechanism();
	{
		listOfClassMechanisms.add(genericClassMechanism);
		listOfClassMechanisms.add(noopMechanism);
		listOfClassMechanisms.addAll(Mechanisms.getClassMechanisms());

		listOfAttributeMechanisms.add(genericAttributeMechanism);
		listOfAttributeMechanisms.add(noopMechanism);
		listOfAttributeMechanisms.addAll(Mechanisms.getAttributeMechanisms());

		listOfReferenceMechanisms.add(genericReferenceMechanism);
		listOfReferenceMechanisms.add(noopMechanism);
		listOfReferenceMechanisms.addAll(Mechanisms.getReferenceMechanisms());

		listOfContainmentMechanisms.add(genericReferenceMechanism);
		listOfContainmentMechanisms.add(noopMechanism);
		listOfContainmentMechanisms.addAll(Mechanisms.getContainmentMechanisms());
	}

	public Mechanism getMechanism(String byName) {
		final LinkedList<Mechanism> allMechanisms = new LinkedList<>();
		allMechanisms.addAll(listOfClassMechanisms);
		allMechanisms.addAll(listOfAttributeMechanisms);
		allMechanisms.addAll(listOfReferenceMechanisms);
		allMechanisms.addAll(listOfContainmentMechanisms);
		try {
			return getMechanism(byName, allMechanisms);
		} catch (IllegalArgumentException e) {
			model.addElement("Could not find Mechanism class with the name [" + byName + "]");
			return noopMechanism;
		}
	}

	private Mechanism getMechanism(String byName, List<Mechanism> listOfMechanisms) {
		for (final Mechanism mechanism : listOfMechanisms) {
			if (mechanism.getName().equals(byName))
				return mechanism;
		}
		throw new IllegalArgumentException("The list of mechanisms does not know about the mechanism " + byName);
	}
}
