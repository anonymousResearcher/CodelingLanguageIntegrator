package integrationMechanism.core.GUI

import org.codeling.mechanisms.ReferenceMechanism
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment

class GenericReferenceMechanism extends ReferenceMechanism {

	override getName() {
		return "Custom Translation";
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EReference eReference = element as EReference;
		val String eReferenceInstantiation = eReference.EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			
			import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import org.codeling.utils.CodelingException;
			import «eReference.packageName».«eReference.packageName»Package;
			import «eReference.packageName».«eReference.EContainingClass.name»;
			import «eReference.packageName».«eReference.EType.name»;
			
			public class «typeName»Transformation
					extends ReferenceMechanismTransformation<«eReference.EContainingClass.name», «eReference.EType.name», IJavaElement> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, IJavaElement> parentTransformation) {
					super(parentTransformation, «eReferenceInstantiation»);
				}
			
				@Override
					public void doCreateCrossReferencesTransformations(
							List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected void doCreateChildTransformationsToCode(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
				
				@Override
				public void createCodeFragments() throws CodelingException {
					// TODO Auto-generated method stub
					
				}
			
				@Override
				public void updateCodeFragments() throws CodelingException {
					// TODO Auto-generated method stub
					
				}
			
				@Override
				public void deleteCodeFragments() throws CodelingException {
					// TODO Auto-generated method stub
					
				}
			
				@Override
				public «eReference.EContainingClass.name» transformToModel() throws CodelingException {
					// TODO Auto-generated method stub
					return null;
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return true;
	}

}
