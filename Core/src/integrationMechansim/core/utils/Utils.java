package integrationMechansim.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.tree.TreeNode;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;

import integrationMechanism.core.GUI.DisabledItemsComboBox;
import integrationMechanism.core.generator.EObjectTreeNode;
import integrationMechanism.core.generator.IntegrationMechanismCodeGenerator;

public class Utils {

	public static IPackageFragmentRoot createRootPackage(IJavaProject project) throws CoreException {
		// create folder by using resources package
		IFolder folder = project.getProject().getFolder("src/");
		folder.create(true, true, null);
		folder = project.getProject().getFolder("src/main");
		folder.create(true, true, null);
		folder = project.getProject().getFolder("src/main/java");
		folder.create(true, true, null);
		// create packages
		IPackageFragmentRoot rootPack = ((IJavaProject) project).getPackageFragmentRoot(folder);
		return rootPack;
	}

	public static IJavaProject createProject(String name) throws CoreException {
		IProject proj = getProject(name);
		if (proj != null)
			return JavaCore.create(proj);

		IJavaProject project = createJavaProject(name);

		// set the build path
		IClasspathEntry[] buildPath = {
				JavaCore.newSourceEntry(project.getProject().getFullPath().append("src/main/java")),
				JavaRuntime.getDefaultJREContainerEntry() };

		project.setRawClasspath(buildPath, project.getProject().getFullPath().append("target/classes"), null);

		return project;

	}

	public static IProject getProject(String name) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(name);
		if (!project.exists()) {
			return null;
		}
		return project;
	}

	public static IJavaProject createJavaProject(String projectName) throws CoreException {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(projectName);
		if (!project.exists()) {
			project.create(null);
		} else {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		}

		if (!project.isOpen()) {
			project.open(null);
		}

		IPath outputLocation;			
			IFolder folder = project.getProject().getFolder("target/");
			folder.create(true, true, null);
			folder = project.getProject().getFolder("target/classes");
			folder.create(true, true, null);	
			outputLocation = folder.getFullPath();
	

		if (!project.hasNature(JavaCore.NATURE_ID)) {
			addNatureToProject(project, JavaCore.NATURE_ID, null);
		}

		IJavaProject jproject = JavaCore.create(project);

		jproject.setOutputLocation(outputLocation, null);
		jproject.setRawClasspath(new IClasspathEntry[0], null);

		return jproject;
	}

	private static void addNatureToProject(IProject proj, String natureId, IProgressMonitor monitor)
			throws CoreException {
		IProjectDescription description = proj.getDescription();
		String[] prevNatures = description.getNatureIds();
		String[] newNatures = new String[prevNatures.length + 1];
		System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
		newNatures[prevNatures.length] = natureId;
		description.setNatureIds(newNatures);
		proj.setDescription(description, monitor);
	}

	/*
	 * public static TreeNode getNodeForComboBox(DisabledItemsComboBox box,
	 * TreeNode node) { if (node instanceof EObjectTreeNode) { if
	 * (((EObjectTreeNode) node).getTypeSelectionGUI() == box) { return node; }
	 * } for (int i = 0; i < node.getChildCount(); i++) { if
	 * (getNodeForComboBox(box, node.getChildAt(i)) != null) return
	 * getNodeForComboBox(box, node.getChildAt(i)); }
	 * 
	 * return null; }
	 */

	public static TreeNode getNodeForComboBox(DisabledItemsComboBox box, TreeNode node,
			HashMap<EObjectTreeNode, DisabledItemsComboBox> myMap) {
		if (node instanceof EObjectTreeNode) {
			if (myMap.get((EObjectTreeNode) node) == box)
				return node;
		}
		for (int i = 0; i < node.getChildCount(); i++) {
			if (getNodeForComboBox(box, node.getChildAt(i), myMap) != null)
				return getNodeForComboBox(box, node.getChildAt(i), myMap);
		}

		return null;
	}

	public static ArrayList<EObjectTreeNode> getAllChildNotes(TreeNode node) {
		ArrayList<EObjectTreeNode> nodeList = new ArrayList<EObjectTreeNode>();
		for (int i = 0; i < node.getChildCount(); i++) {
			if (node.getChildAt(i) instanceof EObjectTreeNode)
				nodeList.add((EObjectTreeNode) node.getChildAt(i));
			nodeList.addAll(getAllChildNotes(node.getChildAt(i)));
		}
		return nodeList;
	}

	public static String readFile(String x) throws URISyntaxException, IOException {
		String txt = "";
		InputStream res = IntegrationMechanismCodeGenerator.class.getResourceAsStream(x);

		BufferedReader reader = new BufferedReader(new InputStreamReader(res));
		String line = null;
		while ((line = reader.readLine()) != null) {
			txt += line;
			txt += "\n";
		}
		reader.close();
		return txt;
	}

	public static EObject getParentObject(TreeNode node) {
		EObject parentObject;
		TreeNode parentNode = node.getParent();
		while ((!(parentNode instanceof EObjectTreeNode))) {
			parentNode = parentNode.getParent();
			if (node == null)
				return null;
		}
		parentObject = ((EObjectTreeNode) parentNode).getObjectToHold();
		return parentObject;

	}

	public static EObjectTreeNode getNodeForEObject(EObject eobject, Map<EObjectTreeNode, DisabledItemsComboBox> map){
		for(EObjectTreeNode key : map.keySet()){
			if(key.getObjectToHold() == eobject)
				return key;
			
		}
		return null;
		
		
	}
	
}
