package im.Handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import integrationMechanism.core.generator.IntegrationMechanismCodeGenerator;

public class MenuChoiceHandler implements IHandler {

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IntegrationMechanismCodeGenerator myTest = new IntegrationMechanismCodeGenerator();

		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		final IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
		final IFile firstElement = (IFile) selection.getFirstElement();
		myTest.loadModel(firstElement.getFullPath().toPortableString());
		return null;
	}

	public static Object getVariable(ExecutionEvent event, String name) {
		if (event.getApplicationContext() instanceof IEvaluationContext) {
			final Object var = ((IEvaluationContext) event.getApplicationContext()).getVariable(name);
			return var == IEvaluationContext.UNDEFINED_VARIABLE ? null : var;
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isHandled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

}
